

const express = require('express');
//import express module in the application


const app = express()	//createServer()
// express() is the server stored in the variable app

const PORT = 3005;

// Middlewares
app.use(express.json())
app.use(express.urlencoded({extended:true}))


// HANDLING ROUTES WITH HTTP METHOD
// app.method(<endpoint>, <request lister function>)
app.get("/", (req, res) => res.status(202).send("Hello from get route") )


app.post("/hello", (req, res) => {

	// mini activity:
		//refactor this by using request body to show dynamic output on the response by showing dynamic names. 

	console.log(req.body)	//{ firstName: 'Chester', lastName: 'Clavio' }

	// console.log(typeof req.body)
	const {firstName, lastName} = req.body


	res.send(`Hello, my name is ${firstName} ${lastName}!`)
	
})


// Mini activity:
	// create a route for "/signup" endpoint that will add a document to a mock database. 

	//if username and password are not empty, add this new document in the mock database, then send a response "user <name> successfully registered"
	//else send back a response "please input both username and password"
	//send a screenshot to group chat


const user = [
	{
		"userName": "joypague",
		"password": "123"
	},
	{
		"userName": "anghelito",
		"password": "456"
	}
];
// (create your own mock database)

app.post("/signup", (req, res) => {
	// create a route for "/signup" endpoint that will add a document to a mock database. 
	const {userName, password} = req.body

	if(userName !== "" && password !== ""){
		//if username and password are not empty, add this new document in the mock database

		user.push(req.body)
		//new document added to mock database
		console.log(user)

		res.send(`User ${userName} successfully registered.`)
		//then send a response "user <name> successfully registered"

	} else {
		//else send back a response "please input both username and password"

		res.status(400).send(`Please input both Username and Password.`)
	}
})


/*Mini Activity
	Create a route /change-password that will be able to update the password and return a message User <name>'s password has been updated. Else, User does not exist.
	Clue: use loop and assign the new pw from the old pw.
	
	check using console.log if password has been updated.
	
	screenshot your answer and send to gc.
*/
app.patch('/change-password', (req, res) => {
	// console.log(req)
	const {userName, password} = req.body

	let message;

	for(let i = 0; i < user.length; i++){

		if(user[i].userName === userName){
			user[i].password = password

			message = `User ${userName}'s password has been updated`

			console.log(user)

			break;

		} else {

			message = `User does not exist`
		}
	}


	res.status(200).send(message)
})



// LISTEN METHOD
app.listen(PORT, () => console.log(`Server connected to port ${PORT}`))